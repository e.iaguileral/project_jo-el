import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalcTest {

	@Test
	void test() {
		int casos = 2;
		String [] array = new String [casos];
		array[0] = "0 * 5";
		array[1] = "0 + 0";
		int[] resultado = new int[casos];
		resultado[0] = 0;
		resultado[1] = 0;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test2() {
		int casos = 3;
		String [] array = new String [casos];
		array[0] = "-2 * 6";
		array[1] = "-7 * -7";
		array[2] = "-8 / -2";
		int[] resultado = new int[casos];
		resultado[0] = -12;
		resultado[1] = 49;
		resultado[2] = 4;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test3() {
		int casos = 0;
		String [] array = new String [casos];
		int[] resultado = new int[casos];
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test4() {
		int casos = 2;
		String [] array = new String [casos];
		array[0] = "1 / 8";
		array[1] = "5 / 5";
		int[] resultado = new int[casos];
		resultado[0] = 0;
		resultado[1] = 1;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test5() {
		int casos = 4;
		String [] array = new String [casos];
		array[0] = "0 + 0";
		array[1] = "0 - 0";
		array[2] = "0 * 0";
		array[3] = "0 / 10";
		int[] resultado = new int[casos];
		resultado[0] = 0;
		resultado[1] = 0;
		resultado[2] = 0;
		resultado[3] = 0;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test6() {
		int casos = 3;
		String [] array = new String [casos];
		array[0] = "16 / 4";
		array[1] = "52 / -21";
		array[2] = "-5 / 5";
		int[] resultado = new int[casos];
		resultado[0] = 4;
		resultado[1] = -2;
		resultado[2] = -1;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test7() {
		int casos = 2;
		String [] array = new String [casos];
		array[0] = "15 / -15";
		array[1] = "-15 * 0";
		int[] resultado = new int[casos];
		resultado[0] = -1;
		resultado[1] = 0;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
	
	@Test
	void test8() {
		int casos = 4;
		String [] array = new String [casos];
		array[0] = "6 - -7";
		array[1] = "20 + -5";
		array[2] = "4 - 20";
		array[3] = "-30 + -50";
		int[] resultado = new int[casos];
		resultado[0] = 13;
		resultado[1] = 15;
		resultado[2] = -16;
		resultado[3] = -80;
		int[] resultadof = Calculadora.programa(array,casos);
		assertArrayEquals(resultado,resultadof);
	}
}
