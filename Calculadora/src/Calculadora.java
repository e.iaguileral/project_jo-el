/**
 * @author IkerAguilera
 * @version 1.0
 */
import java.util.Scanner;
public class Calculadora {

	static Scanner sc = new Scanner(System.in);
	
	/**
	 * Funció "main" perquè la persona indiqui totes les dades, en aquest cas demana la grandària de l'array
	 * on emmagatzemaràs totes les operacions i amb un bucle afegeix a l'array les diferents operacions entre
	 * dos números en forma de cadena.
	 */
	public static void main(String[] args) {
		int casos = sc.nextInt();
		sc.nextLine();
		String [] array = new String [casos]; 
		for(int i = 0; i<casos;i++) {
			array[i] = sc.nextLine();
		}
		programa(array,casos);
	}
	/**
	 * La función programa crea otro array que contendrá los resultados de cada operación.
	 * Acto seguido realiza un bucle. En cada bucle se hace un split de cada operación para ver el signo del centro.
	 * @param array, Array que conté les operacions indicades al main.
	 * @param casos, Longitud de l'array.
	 * @return nums, Array amb els resultats de les operacions.
	 */
	public static int[] programa(String[] array,int casos) {
		int[] nums = new int[casos];
		for(int i = 0; i<casos;i++) {
			String[] split = array[i].split(" ");
			switch(split[1]) {
			/**
			 * Si el signe de l'operació en l'actual iteració del bucle és un "+", 
			 * es realitza una suma entre els números de l'operació i es guarda a l'array "nums".
			 */
			case "+":
				nums[i] = Integer.parseInt(split[0]) + Integer.parseInt(split[2]);
				break;
			/**
			 * Si el signe de l'operació en l'actual iteració del bucle és un "-", 
			 * es realitza una resta entre els números de l'operació i es guarda a l'array "nums".
			 */
			case "-":
				nums[i] = Integer.parseInt(split[0]) - Integer.parseInt(split[2]);
				break;
			/**
			 * Si el signe de l'operació en l'actual iteració del bucle és un "*", 
			 * es realitza una multiplicació entre els números de l'operació i es guarda a l'array "nums".
			 */
			case "*":
				nums[i] = Integer.parseInt(split[0]) * Integer.parseInt(split[2]);
				break;
			/**
			 * Si el signe de l'operació en l'actual iteració del bucle és un "/", 
			 * es realitza una divisió entre els números de l'operació i es guarda a l'array "nums".
			 */
			case "/":
				nums[i] = Integer.parseInt(split[0]) / Integer.parseInt(split[2]);
				break;
			}
		}
		for(int i = 0;i<nums.length;i++) {
			System.out.println(nums[i]);
		}
		return nums;
	}
	
}
